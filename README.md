# Flow MVC
Just a simple MVC example created from scratch using PHP.

## Instructions
### Installation

Run `composer dump-autoload` from the console in the project's root directory.

Import `users_sample.sql` into MySQL database if you want to test it with some sample data or import `users.sql` if you want to test it on empty table.

In the `index.php` file, set your database parameters.

Access the website using the `localhost` URL.

### Routes

+ `GET | http://localhost/` - Add a new user into the database view
+ `GET | http://localhost/list` - List all users view
+ `POST | http://localhost/insert` - Inserts new user into database

# Notice

Every part of this application is written from scratch. No frameworks or composer packages are used in development of this project.

Author: **Jovan Ivanovic**