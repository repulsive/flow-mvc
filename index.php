<?php

require __DIR__ . '/vendor/autoload.php';

$app = new App\Core\Bootstrap();

/**
 * Set root directory
 */
$app->setRootDirectory(__DIR__);

/**
 * Setting database parameters
 */
$app->setDatabaseParameter('TYPE', 'mysql')     // Type of database
    ->setDatabaseParameter('HOST', 'localhost') // Hostname
    ->setDatabaseParameter('NAME', 'flow_mvc')  // Name of database
    ->setDatabaseParameter('USER', 'root')      // Username
    ->setDatabaseParameter('PASS', '');         // Password

/**
 * Defining routes
 */
$app->router
    ->defineRoute('GET', '/', 'App\Controllers\UsersController@create')
    ->defineRoute('GET', '/list', 'App\Controllers\UsersController@index')
    ->defineRoute('POST', '/insert', 'App\Controllers\UsersController@store');

/**
 * Starts the main app cycle
 */
$app->boot();