<?php

namespace App\Core;

class View
{
    public static $data;

    /**
     * Render view and show it to the user
     *
     * @param string $view
     * @param array $pre
     * @param array $post
     * @param array $data
     */
    public static function render(string $view, array $pre, array $post, array $data = [])
    {
        self::$data = $data;
        
        foreach ($pre as $_view)
        {
            include_once ROOT_DIR.'/App/Views/'.$_view.'.php';
        }
        
        require_once ROOT_DIR.'/App/Views/'.$view.'.php';
        
        foreach ($post as $_view)
        {
            include_once ROOT_DIR.'/App/Views/'.$_view.'.php';
        }
    }

    /**
     * Redirect to an location
     *
     * @param string $location
     * @param bool $permanent
     */
    public static function redirect(string $location, bool $permanent = false)
    {
        header('Location: '.$location, true, $permanent ? 301 : 302);

        exit();
    }
}