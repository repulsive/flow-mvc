<?php

namespace App\Core\Routing;

use App\Core\Http\Request;

class Router
{
    public $routeCollection;
    public $uri;

    protected $request;

    public function __construct()
    {
        $this->routeCollection = new RouteCollection();
    }

    /**
     * Initializes router to start getting requests
     *
     * @return $this
     * @throws RouteNotFoundException
     * @throws \App\Core\Exceptions\MethodNotAllowedException
     */
    public function init()
    {
        $this->request = new Request();

        $route = $this->routeCollection->find($this->request->method, $this->request->uri);

        if ($route !== null)
        {
            $this->executeRouteAction($route);
        }

        return $this;
    }

    /**
     * Adds route into RouteCollection
     *
     * @param $method
     * @param $path
     * @param $action
     * @param null $name
     * @return $this
     */
    public function defineRoute($method, $path, $action, $name = null)
    {
        $route = new Route($method, $path);
        $route->setAction($action);

        $this->routeCollection->add($route);

        return $this;
    }

    /**
     * Executes route's specified action
     *
     * @param Route $route
     * @return mixed
     */
    public function executeRouteAction(Route $route)
    {
        $instance = new $route->action['class']($this->request);
        return $instance->{$route->action['method']}(...$route->parameters);
    }
}