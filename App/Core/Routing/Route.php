<?php

namespace App\Core\Routing;

class Route
{
    /**
     * Request method required for this route
     *
     * @var
     */
    public $method;

    /**
     * Route's uri
     *
     * @var
     */
    public $uri;

    /**
     * Action to execute when the route is accessed
     *
     * @var array
     */
    public $action = [];

    /**
     * Name of the route
     *
     * @var null
     */
    public $name = null;

    /**
     * Route parameters
     *
     * @var null
     */
    public $parameters = null;

    /**
     * Route constructor.
     * Set request method and the route uri properties
     * 
     * @param $method
     * @param $uri
     */
    public function __construct($method, $uri)
    {
        $this->method = $method;
        $this->uri = $uri;
    }

    /**
     * Sets an action to the Route
     *
     * @param $action
     * @return $this
     */
    public function setAction($action)
    {
        $action = explode('@', $action);

        $this->action = [
            'class' => $action[0],
            'method' => $action[1]
        ];

        return $this;
    }

    /**
     * Sets a name to the Route
     *
     * @param $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Checks if Route has URL parameters
     *
     * @return bool
     */
    public function hasParameters() : bool
    {
        return ($this->parameters !== null) ? true : false;
    }

    /**
     * Sets Route's parameters
     *
     * @param $parameters
     */
    public function setParameters($parameters)
    {
        $this->parameters = $parameters;
    }
}