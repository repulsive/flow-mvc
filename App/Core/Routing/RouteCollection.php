<?php

namespace App\Core\Routing;

use App\Core\Exceptions\Exception;
use App\Core\Exceptions\MethodNotAllowedException;
use App\Core\Exceptions\RouteNotFoundException;

class RouteCollection
{
    /**
     * List of all defined routes
     *
     * @var array
     */
    public $routes = [];

    /**
     * Looks for a specific route by method and uri in the RouteCollection
     *
     * @param $method
     * @param $uri
     * @return mixed|null
     */
    public function find($method, $uri)
    {
        try
        {
            foreach ($this->routes as $route)
            {
                $regex_uri = preg_replace('/([{][a-zA-Z]*[}])/i', '(\w+)', $route->uri);
                $pattern = '/^' . str_replace('/', '\/', $regex_uri) . '$/';

                if (preg_match($pattern, $uri, $parameters))
                {
                    array_shift($parameters);

                    if ($route->method === $method)
                    {
                        $route->setParameters($parameters);
                        return $route;
                    }
                    else
                    {
                        throw new MethodNotAllowedException();
                    }
                }
            }

            throw new RouteNotFoundException();
        }
        catch (Exception $e)
        {
            $e->print(true);
            return null;
        }
    }

    /**
     * Adds a new route to the RouteCollection
     *
     * @param Route $route
     * @return $this
     */
    public function add(Route $route)
    {
        array_push($this->routes, $route);

        return $this;
    }
}