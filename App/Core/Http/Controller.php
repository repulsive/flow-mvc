<?php

namespace App\Core\Http;

abstract class Controller
{
    /**
     * Instance of Request class
     *
     * @var Request
     */
    public $request;
    
    public function __construct(Request $request)
    {
        $this->request = $request;
    }
}