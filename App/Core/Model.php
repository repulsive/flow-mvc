<?php

namespace App\Core;

use App\Core\Database;

abstract class Model
{
    /**
     * Database instance
     *
     * @var mixed
     */
    public $database;

    /**
     * Name of the model's table
     *
     * @var string
     */
    protected $table = '';

    /**
     * Fields of the model
     *
     * @var array
     */
    protected $fields = [];

    public function __construct()
    {
        $this->database = Database::getInstance();
    }

    /**
     * Get all instances of the model
     *
     * @return mixed
     */
    public function all()
    {
        $query = $this->database->prepare("SELECT * FROM `{$this->table}`");
        $query->execute();

        return $query->fetchAll();
    }

    /**
     * Insert a new model into database
     *
     * @param array $fields
     * @return array
     */
    public function insert(array $fields)
    {
        $sql = "INSERT INTO `{$this->table}` (";
        $sql .= '`'.implode('`,`', array_keys($fields)).'`) VALUES(';
        $sql .= '"'.implode('","', $fields).'")';

        $query = $this->database->prepare($sql);
        $query->execute();

        return $fields;
    }
}