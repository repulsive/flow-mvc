<?php

namespace App\Controllers;

use App\Core\Http\Controller;
use App\Core\Provider;
use App\Core\View;
use App\Models\User;

class UsersController extends Controller
{
    /**
     * Show all users
     */
    public function index()
    {
        $users = new User();
        $users = $users->all();

        View::render('list',
            ['layouts/header'],
            ['layouts/footer'],
            ['users' => $users]
        );
    }

    /**
     * Show a form to store a new user
     */
    public function create()
    {
        View::render('create',
            ['layouts/header'],
            ['layouts/footer']
        );
    }
    
    /**
     * Store a new user into database
     */
    public function store()
    {
        $user = new User();
        $user = $user->insert([
            'first_name' => $this->request->post('first_name'),
            'last_name' => $this->request->post('last_name'),
            'street_number' => $this->request->post('street_number'),
            'city' => $this->request->post('city'),
            'country' => $this->request->post('country'),
        ]);

        View::redirect('/list');
    }
}