</div>
<div class="column" id="map" size="6">

</div>
</div>

<script>
    function checkForm(e) {
        var first_name = document.forms['create']['first_name'];
        var last_name = document.forms['create']['last_name'];
        var street_number = document.forms['create']['street_number'];
        var city = document.forms['create']['city'];
        var country = document.forms['create']['country'];

        if (first_name.value == "") {
            document.getElementById('label-first_name').innerHTML = "Please enter First Name";
            first_name.classList.add('error');
        } else {
            document.getElementById('label-first_name').innerHTML = "";
            first_name.classList.remove('error');
        }
        if (last_name.value == "") {
            document.getElementById('label-last_name').innerHTML = "Please enter Last Name";
            last_name.classList.add('error');
        } else {
            document.getElementById('label-last_name').innerHTML = "";
            last_name.classList.remove('error');
        }
        if (street_number.value == "") {
            document.getElementById('label-street_number').innerHTML = "Please enter Street / Number";
            street_number.classList.add('error');
        } else {
            document.getElementById('label-street_number').innerHTML = "";
            street_number.classList.remove('error');
        }
        if (city.value == "") {
            document.getElementById('label-city').innerHTML = "Please enter City";
            city.classList.add('error');
        } else {
            document.getElementById('label-city').innerHTML = "";
            city.classList.remove('error');
        }
        if (country.value == "") {
            document.getElementById('label-country').innerHTML = "Please enter Country";
            country.classList.add('error');
        } else {
            document.getElementById('label-country').innerHTML = "";
            country.classList.remove('error');
        }

        if (first_name.value != "" && last_name.value != "" && street_number.value != "" && city.value != "" && country.value != "") {
            document.forms['create'].submit();
        }
    }

    function initMap() {
        window.defaultLatLng = new google.maps.LatLng(45.805499, 9.054902);

        window.map = new google.maps.Map(document.getElementById('map'), {
            center: window.defaultLatLng,
            zoom: 14
        });

        window.geocoder = new google.maps.Geocoder();

        window.marker = new google.maps.Marker({ map: window.map });
    }

    function generateQuery(street_number, city, country) {
        var arr = [];

        if (street_number)
            arr.push(street_number);

        if (city)
            arr.push(city);

        if (country)
            arr.push(country);

        return arr.join(',');
    }

    function changeMarker(handler = null) {
        if (handler !== null) {
            var query = handler.childNodes[3].innerHTML;

            var elements = document.getElementsByClassName('item');

            for (var i = 0; i < elements.length; i++) {
                elements[i].classList.remove('selected');
            };

            handler.classList.toggle('selected');
        } else {
            var street_number = document.getElementById("street_number").value;
            var city = document.getElementById("city").value;
            var country = document.getElementById("country").value;

            var query = generateQuery(street_number, city, country);
        }

        console.log(query);

        if (window.geocoder) {
            window.geocoder.geocode({ 'address': query }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
                        window.map.setCenter(results[0].geometry.location);

                        var infowindow = new google.maps.InfoWindow({
                            content: '<b>' + query + '</b>',
                            size: new google.maps.Size(150,50)
                        });

                        window.marker.setPosition(results[0].geometry.location);

                        google.maps.event.addListener(marker, 'click', function() {
                            infowindow.open(window.map, marker);
                        });

                    } else {
                        alert("No results found");
                    }
                } else {
                    window.map.setCenter(window.defaultLatLng);
                }
            });
        }
    }
</script>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCa9dYQ4MfLiAXhOHtXyNy0XEv1Nan4TYQ&callback=initMap"></script>
</body>
</html>