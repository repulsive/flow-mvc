<h1>All Users <a href="/">Add new user</a></h1>
<div class="list">
    <?php foreach(self::$data['users'] as $user): ?>
        <div class="item" onclick="changeMarker(this)">
            <span class="name"><?= $user['first_name'].' '.$user['last_name'] ?></span>
            <span class="location"><?= $user['street_number'].', '.$user['city'].', '.$user['country'] ?></span>
        </div>
    <?php endforeach; ?>
</div>