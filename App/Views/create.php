<h1>User Details</h1>
<form name="create" method="POST" action="/insert">
    <div class="form-control half">
        <label id="label-first_name"></label>
        <input formnovalidate required type="text" name="first_name" id="first_name" placeholder="First Name" />
    </div>

    <div class="form-control half">
        <label id="label-last_name"></label>
        <input formnovalidate required type="text" name="last_name" id="last_name" placeholder="Last Name" />
    </div>

    <div class="form-control">
        <label id="label-street_number"></label>
        <input formnovalidate required type="text" name="street_number" id="street_number" placeholder="Street / Number" onkeyup="changeMarker()" />
    </div>

    <div class="form-control">
        <label id="label-city"></label>
        <input formnovalidate required type="text" name="city" id="city" placeholder="City" onkeyup="changeMarker()" />
    </div>

    <div class="form-control">
        <label id="label-country"></label>
        <input formnovalidate required type="text" name="country" id="country" placeholder="Country" onkeyup="changeMarker()" />
    </div>

    <button type="button" onclick="checkForm()">Add User</button>
</form>