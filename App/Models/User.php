<?php

namespace App\Models;

use App\Core\Model;

class User extends Model
{
    protected $table = 'users';
    
    protected $fields = [
        'first_name', 'last_name', 'street_number', 'city', 'country'
    ];
}